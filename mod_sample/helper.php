<?php
/**
* @package 		mod_sample - Sample Module
* @version		1.0.0
* @created		Sep 2020
* @author		ExtensionPlazza.com
* @email		support@extensionplazza.com
* @website		https://www.extensionplazza.com
* @support		https://www.extensionplazza.com/support..html
* @copyright	Copyright (C) 2020 - 2021 https://www.extensionplazza.com. All rights reserved.
* @license		GNU General Public License version 2 and above
*/
defined('_JEXEC') or die;

// class
class ModSampleHelper
{
	public static function getIpAddress( $params )
	{
		if (!empty($_SERVER['HTTP_CLIENT_IP']))
		{
			//check ip from share internet
		  $ip = $_SERVER['HTTP_CLIENT_IP'];
		}
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   
		{
			//check ip is pass from proxy
		  $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else
		{
		  $ip = $_SERVER['REMOTE_ADDR'];
		}
	 
		return $ip;
	}
}
// added at bb
// added at pc
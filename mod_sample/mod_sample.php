<?php
/**
* @package 		mod_sample - Sample Module
* @version		1.0.0
* @created		Sep 2020
* @author		ExtensionPlazza.com
* @email		support@extensionplazza.com
* @website		https://www.extensionplazza.com
* @support		https://www.extensionplazza.com/support..html
* @copyright	Copyright (C) 2020 - 2021 https://www.extensionplazza.com. All rights reserved.
* @license		GNU General Public License version 2 and above
*/
defined('_JEXEC') or die;

	// modules base location
	$site_uri = JUri::base();
	$current_uri = JUri::current();
	$module_uri = $site_uri.'modules/mod_sample/';

		$document = JFactory::getDocument();
		// add module's stylesheet to <head>
		$document->addStyleSheet($module_uri.'assets/css/style.css');
		// add module's js file to <head>
		//$document->addScript($module_uri.'assets/js/javascript.js');

		// register Helper Class from helper.php 
		JLoader::register('ModSampleHelper', __DIR__ . '/helper.php');
		// Assign getData Method of Helper Class to $get_data variable
		$reg_ip  = ModSampleHelper::getIpAddress($params);
		$mod_id = $module->id;
		$uri = JURI::getInstance();		
		// assign parameters to variables 
		$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'), ENT_COMPAT, 'UTF-8');
		// Field : Show on Mobile 
		$showonmobile 			= $params->get('showonmobile', '1');
		// Field : Hello Text
		$hellotext 			= $params->get('hellotext', 'Hello World');

		// Load Layout 
		require JModuleHelper::getLayoutPath('mod_sample', 'theme');